<?php

namespace App\Console\Commands;

use App\Models\Device;
use App\Models\Stat;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CheckDevices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'devices:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check installed devices for today';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $devices_installed = Device::installed()->count();
        $devices_removed = Device::removed()->count();
        $now = Carbon::now()->toDateString();
        if($stat = Stat::where('date', '=', $now)->first()) {
            $stat->update([ 'installed' => $devices_installed, 'removed' => $devices_removed ]);
        } else {
            $stat = Stat::create([
                'installed' => $devices_installed,
                'removed' => $devices_removed,
                'date' => $now
            ]);
        }

    }
}
