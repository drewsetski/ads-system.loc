<?php

namespace App\Http\Controllers;

use App\Models\Settings;
use Illuminate\Http\Request;

use App\Http\Requests;
use Mockery\CountValidator\Exception;

class SettingsController extends Controller
{
    /**
     * Settings index page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $settings = Settings::first();
        $title = 'Settings';
        return view('settings.index', compact('settings', 'title'));
    }

    /**
     * Update settings
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        if($request->ajax()) {
            try {
                Settings::first()->update([$request->input('option') => $request->input('value')]);
                return 'Successfully updated';
            } catch (Exception $ex) {
                return $ex->getMessage();
            }

        } else {
            Settings::first()->update($request->all());
            \Flash::success('Settings have been updated');
            return redirect()->back();
        }

    }
}
