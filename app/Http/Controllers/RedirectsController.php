<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Redirect;
use App\Models\Settings;
use Illuminate\Http\Request;

use App\Http\Requests;
use Laracasts\Flash\Flash;

class RedirectsController extends Controller
{
    /**
     * Redirects index page. Displays all redirects
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $redirects = Redirect::orderBy('created_at','desc')->get();
        $settings = Settings::first();
        $title = 'Redirects';
        return view('redirects.index', compact('redirects', 'title', 'settings'));
    }

    /**
     * Get all redirects
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function redirects()
    {
        return response()->json(Redirect::with('country')->get());
    }

    /**
     * Display page for creating new alert
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $title = 'Create redirect';
        $countries = Country::lists('name', 'id');
        $redirect = null;
        $url = 'redirects/add'; // url for form submitting
        return view('redirects.edit', compact('title', 'redirect', 'url', 'countries'));
    }

    /**
     * Store newly created alert in database
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $redirect = new Redirect($request->only('link'));
        $country = Country::find(['id' => $request->input('country')])->first();
        $country->redirects()->save($redirect);
        Flash::success('Redirect has been successfully created');
        return redirect('redirects');
    }

    /**
     * Display edit page for existing alert
     *
     * @param Redirect $redirect
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Redirect $redirect)
    {
        $title = 'Edit alert';
        $url = 'redirects/edit/' . $redirect->id; // url for form submitting
        $countries = Country::lists('name', 'id');
        return view('redirects.edit', compact('title', 'redirect', 'url', 'countries'));
    }

    /**
     * Update existing alert
     *
     * @param Redirect $redirect
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Redirect $redirect, Request $request)
    {
        $redirect->link = $request->input('link');
        $country = Country::find(['id' => $request->input('country')])->first();
        $country->redirects()->save($redirect);;
        Flash::success('Alert has been successfully updated');
        return redirect('redirects');
    }

    /**
     * Delete existing redirect
     *
     * @param Redirect $redirect
     * @throws \Exception
     */
    public function delete(Redirect $redirect)
    {
        $redirect->delete();
    }

}
