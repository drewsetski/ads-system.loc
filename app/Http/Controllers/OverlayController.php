<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Overlay;
use Illuminate\Http\Request;

use App\Http\Requests;

class OverlayController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show view for overlays
     */
    public function getIndex()
    {
        $title = "Overlays";
        $overlay_count = Overlay::count();

        return view('overlays/overlays', compact(['title', 'overlay_count']));
    }

    /**
     * Get all ads
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGet(Request $request)
    {
        $offset = $request->get('offset');
        $limit = $request->get('limit');
        $search = $request->get('search');
        if($search) {
            $rows = Overlay::whereHas('countries', function ($query) use ($search) {
                $query->where('name', 'like', '%' . $search . '%')
                    ->orWhere('code', 'like', '%' . $search . '%');
            })
                ->orWhere('overlays.slug', 'like', '%' . $search . '%')
                ->with(array('countries' =>function($query){
                    $query->select('name');
                }))
                ->orderBy('created_at','desc')
                ->skip($offset)
                ->take($limit)
                ->get();
            $total = count($rows);
        } else {
            $rows = Overlay::with(array('countries' =>function($query){
                $query->select('name');
            }))
                ->orderBy('created_at','desc')
                ->skip($offset)
                ->take($limit)
                ->get();
            $total = Overlay::all()->count();
        }

        return response()->json(compact('total', 'rows'));
    }

    /**
     * Show view for creating new home url
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAdd()
    {
        $title = 'Creating overlay';
        $post = new Overlay;
        $countries = Country::all();
        $selected_countries = \DB::table('overlay_country')->lists('country_id');

        return view('overlays.edit', compact([ 'title', 'post', 'countries', 'selected_countries' ]));
    }

    /**
     * Store new home url in database
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postAdd(Request $request)
    {
        $input = array_merge($request->except( ['_token', 'countries', 'banner'] ));
        $overlay = Overlay::create( $input );

        $overlay->countries()->attach($request->get('countries'));
        $country_list = '';
        $countries = Country::whereIn('id', $request->get('countries'))->get();
        foreach($countries as $country) {
            $country_list .= $country->name . ', ';
        }
        $country_list = substr($country_list, 0, -2);
        if( $file = $request->file('banner') ) {
            Overlay::saveFile($file, $overlay->id, false);
        }

        \Flash::success('<div style="font-size: 12pt">Overlay with slug <span class="label label-info">' . $overlay->slug . '</span> has been added successfully for countries: ' . $country_list . '</div>');

        return redirect('/overlays');
    }

    /**
     * Show Overlay for edit
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($id)
    {
        $title = "Editing ad";
        $post = Overlay::findOrFail($id);
        $countries = Country::all();
        $selected_countries = \DB::table('overlay_country')->lists('country_id');

        return view('overlays.edit', compact(['title', 'post', 'countries', 'selected_countries']));
    }

    /**
     * Update ad
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEdit(Request $request, $id)
    {
        $overlay = Overlay::findOrFail($id);
        $overlay->update( $request->except('countries', 'banner') );

        $overlay->countries()->sync($request->get('countries'));

        if( $file = $request->file('banner') ) {
            Overlay::saveFile($file, $overlay->id, true);
        }

        \Flash::success('<div style="font-size: 12pt">Overlay <span class="label label-info">' . $overlay->slug . '</span> has successfully updated</div>');

        return redirect()->back();
    }

    /**
     * Delete ad
     *
     * @param $id
     */
    public function deleteEdit($id)
    {
        Overlay::destroy($id);
    }
}
