<?php

namespace App\Http\Controllers\api;

use App\Models\Ad;
use App\Models\Alert;
use App\Models\Device;
use App\Models\HomeUrl;
use App\Models\Overlay;
use App\Models\Redirect;
use App\Models\Settings;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use GeoIP;

class MainController extends Controller
{
    //================= SWAGGER
    /**
     * @SWG\Post(
     *     path="/api/v1/device",
     *     summary="Add device",
     *     tags={"device"},
     *     description="Add device",
     *     operationId="postDevice",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         name="device_token",
     *         in="formData",
     *         description="Device token for push-notification",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation, device added/updated",
     *     )
     * )
     */
    //================= SWAGGER

    /**
     * Add device
     * Tasks, which user has finished excluded from output.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function device(Request $request)
    {
        $location = GeoIP::getLocation($request->headers->get('x-real-ip'));
        $country_code = $location['isoCode'];
        if ($device = Device::whereDeviceToken($request->input('device_token'))->first()) {
            $device->update(['country_code' => $country_code, 'updated_at' => Carbon::now()->toDateTimeString()]);
        } else {
            $device = Device::create([
                'device_type' => 'android',
                'device_token' => $request->input('device_token'),
                'country_code' => $country_code
            ]);
        }
        $homeurl = HomeUrl::whereHas('countries', function ($query) use ($country_code) {
            $query->where('code', 'like', '%' . $country_code . '%');
        })->first();

        $overlay = Overlay::whereHas('countries', function ($query) use ($country_code) {
            $query->where('code', 'like', '%' . $country_code . '%');
        })->first();

        $alerts = Alert::whereHas('countries', function ($query) use ($country_code) {
                    $query->where('code', 'like', '%' . $country_code . '%');
                })->get();

        $settings = Settings::first();

        $redirect = Redirect::whereHas('country', function ($query) use ($country_code) {
            $query->where('code', 'like', '%' . $country_code . '%');
        })->first();

        $code = 200;
        return response()->json(compact('homeurl', 'overlay', 'alerts', 'redirect', 'settings', 'code'), 200);
    }
    //================= SWAGGER
    /**
     * @SWG\Get(
     *     path="/api/v1/homeurl",
     *     summary="Get home url",
     *     tags={"device"},
     *     description="Get home url for user's country",
     *     operationId="getHomeUrl",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation, home url provided",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Home url not found",
     *     )
     * )
     */
    //================= SWAGGER
    /**
     * Get home url
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function homeurl()
    {
        $location = GeoIP::getLocation(request()->headers->get('x-real-ip'));
        $country_code = $location['isoCode'];

        try {
            $homeurl = HomeUrl::whereHas('countries', function ($query) use ($country_code) {
                $query->where('code', 'like', '%' . $country_code . '%');
            })->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'home_url_not_found'], 404);
        }


        return response()->json(compact('homeurl'), 200);
    }

    //================= SWAGGER
    /**
     * @SWG\Get(
     *     path="/api/v1/overlay",
     *     summary="Get overlay",
     *     tags={"device"},
     *     description="Get overlay for user's country",
     *     operationId="getOverlay",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation, overlay provided",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Overlay not found",
     *     )
     * )
     */
    //================= SWAGGER
    /**
     * Get overlay
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function overlay()
    {
        $location = GeoIP::getLocation(request()->headers->get('x-real-ip'));
        $country_code = $location['isoCode'];

        try {
            $overlay = Overlay::whereHas('countries', function ($query) use ($country_code) {
                $query->where('code', 'like', '%' . $country_code . '%');
            })->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'overlay_not_found'], 404);
        }


        return response()->json(compact('overlay'), 200);
    }

    //================= SWAGGER
    /**
     * @SWG\Get(
     *     path="/api/v1/alerts",
     *     summary="Get alerts",
     *     tags={"alerts"},
     *     description="Get all existing alerts. Periodicity - time in minutes, after which alert will appear",
     *     operationId="getAlerts",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation, home url provided",
     *     ),
     *     @SWG\Response(
     *         response=204,
     *         description="There are no alerts",
     *     )
     * )
     */
    //================= SWAGGER

    /**
     * Get all alerts
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function alerts()
    {
        $alerts = Alert::all();
        $code = $alerts->isEmpty() ? 204 : 200;
        return response()->json(compact('alerts'), $code);
    }

    //================= SWAGGER
    /**
     * @SWG\Get(
     *     path="/api/v1/redirects",
     *     summary="Get redirects",
     *     tags={"redirects"},
     *     description="Get redirects for user's country",
     *     operationId="getRedirects",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation, overlay provided",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Redirects not found",
     *     )
     * )
     */
    //================= SWAGGER

    /**
     * Get redirects
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function redirects()
    {
        $location = GeoIP::getLocation(request()->headers->get('x-real-ip'));
        $country_code = $location['isoCode'];
        try {
            $redirect = Redirect::whereHas('country', function ($query) use ($country_code) {
                $query->where('code', 'like', '%' . $country_code . '%');
            })->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'redirect_not_found'], 404);
        }
        return response()->json(compact('redirect'), 200);
    }

    //================= SWAGGER
    /**
     * @SWG\Get(
     *     path="/api/v1/all",
     *     summary="Get all info",
     *     tags={"all"},
     *     description="Get alerts, redirect, homeurl, overlay",
     *     operationId="getAll",
     *     consumes={"application/xml", "application/json"},
     *     produces={"application/xml", "application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation, all info provided",
     *     )
     * )
     */
    //================= SWAGGER
    /**
     * Get alerts, homeurl, redirect, overlay
     */
    public function getAll()
    {
        $location = GeoIP::getLocation(request()->headers->get('x-real-ip'));
        $country_code = $location['isoCode'];
        $homeurl = HomeUrl::whereHas('countries', function ($query) use ($country_code) {
            $query->where('code', 'like', '%' . $country_code . '%');
        })->first();

        $overlay = Overlay::whereHas('countries', function ($query) use ($country_code) {
            $query->where('code', 'like', '%' . $country_code . '%');
        })->first();

        $alerts = Alert::whereHas('countries', function ($query) use ($country_code) {
            $query->where('code', 'like', '%' . $country_code . '%');
        })->get();

        $settings = Settings::first();

        $redirect = Redirect::whereHas('country', function ($query) use ($country_code) {
            $query->where('code', 'like', '%' . $country_code . '%');
        })->first();

        return response()->json(compact('homeurl', 'overlay', 'alerts', 'redirect', 'settings'), 200);
    }

}
