<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Device;
use App\Models\Stat;
use Artisan;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = env('PROJECT_NAME');
        $now = Carbon::now()->toDateString();
        Artisan::call('devices:check');
        $date = $now;

        return view('home', compact('title', 'stats', 'date'));
    }

    /**
     * Get stats by date
     *
     * @return array
     */
    public function getStats()
    {
        $now = Carbon::now()->toDateString();

        if( request()->has('date') )
            $date = request()->get('date');
        else
            $date = $now;
        $data = array();
        if($stat = Stat::where('date', '=', $date)->first()){
            $data['status'] = 'success';
            $data['date'] = $date;
            $data['installed']  = $stat->installed;
            $data['removed']    = $stat->removed;
        } else {
            $data['status'] = 'failed';
            $data['date'] = $date;
            $data['installed']  = 0;
            $data['removed']    = 0;
        }

        return $data;
    }
}
