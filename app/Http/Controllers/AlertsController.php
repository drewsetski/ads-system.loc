<?php

namespace App\Http\Controllers;

use App\Models\Alert;
use App\Models\Country;
use App\Models\Redirect;
use Illuminate\Http\Request;

use App\Http\Requests;
use Laracasts\Flash\Flash;

class AlertsController extends Controller
{
    /**
     * Alerts index page. Displays all alerts
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        $title = 'Alerts';
        $alerts_count = Alert::count();
        $countries = Country::all();
        return view('alerts.index', compact('alerts_count', 'title', 'countries'));
    }

    /**
     * Get all alerts
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function alerts(Request $request)
    {
        $offset = $request->get('offset');
        $limit = $request->get('limit');
        $search = $request->get('search');
        if($search) {
            $rows = Alert::whereHas('countries', function ($query) use ($search) {
                $query->where('name', 'like', '%' . $search . '%')
                    ->orWhere('code', 'like', '%' . $search . '%');
            })
                ->orWhere('alerts.text', 'like', '%' . $search . '%')
                ->orWhere('alerts.slug', 'like', '%' . $search . '%')
                ->with(array('countries' =>function($query){
                    $query->select('name');
                }))
                ->orderBy('created_at','desc')
                ->skip($offset)
                ->take($limit)
                ->get();
            $total = count($rows);
        } else {
            $rows = Alert::with(array('countries' =>function($query){
                $query->select('name');
            }))
                ->orderBy('created_at','desc')
                ->skip($offset)
                ->take($limit)
                ->get();
            $total = Alert::all()->count();
        }

        return response()->json(compact('total', 'rows'));
    }

    /**
     * Display page for creating new alert
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $title = 'Create alert';
        $alert = new Alert();
        $url = 'alerts/add'; // url for form submitting
        $countries = Country::all();

        return view('alerts.edit', compact('title', 'alert', 'url', 'countries'));
    }

    /**
     * Store newly created alert in database
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $alert = Alert::create($request->except('_token', 'countries'));
        $alert->countries()->attach($request->get('countries'));

        Flash::success('Alert has been successfully created');
        return redirect('alerts');
    }

    /**
     * Display edit page for existing alert
     *
     * @param Alert $alert
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Alert $alert)
    {
        $title = 'Edit alert';
        $url = 'alerts/edit/' . $alert->id; // url for form submitting
        $countries = Country::all();
        return view('alerts.edit', compact('title', 'alert', 'url', 'countries'));
    }

    /**
     * Update existing alert
     *
     * @param Alert $alert
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Alert $alert, Request $request)
    {
        $alert->update($request->except('_token', 'countries'));
        $alert->countries()->sync($request->get('countries'));
        Flash::success('Alert has been successfully updated');
        return redirect('alerts');
    }

    /**
     * Delete existing alert
     *
     * @param Alert $alert
     * @throws \Exception
     */
    public function delete(Alert $alert)
    {
        $alert->delete();
    }

}
