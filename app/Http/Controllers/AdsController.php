<?php

namespace App\Http\Controllers;

use App\Jobs\SendPushNotifications;
use App\Models\Ad;
use App\Models\Country;
use Illuminate\Http\Request;

use App\Http\Requests;

class AdsController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show view for ads
     */
    public function getIndex()
    {
        $title = "Ads";
        $ads_count = Ad::count();
        $countries = Country::all();

        return view('ads/ads', compact(['title', 'ads_count', 'countries']));
    }

    /**
     * Get all ads
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGet(Request $request)
    {
        $offset = $request->get('offset');
        $limit = $request->get('limit');
        $search = $request->get('search');
        if($search) {
            $rows = Ad::whereHas('countries', function ($query) use ($search) {
                                    $query->where('name', 'like', '%' . $search . '%')
                                        ->orWhere('code', 'like', '%' . $search . '%');
                                })
                        ->orWhere('ads.slug', 'like', '%' . $search . '%')
                        ->with(array('countries' =>function($query){
                                    $query->select('name');
                                }))
                        ->orderBy('created_at','desc')
                        ->skip($offset)
                        ->take($limit)
                        ->get();
            $total = count($rows);
        } else {
            $rows = Ad::with(array('countries' =>function($query){
                                $query->select('name');
                            }))
                    ->orderBy('created_at','desc')
                    ->skip($offset)
                    ->take($limit)
                    ->get();
            $total = Ad::all()->count();
        }

        return response()->json(compact('total', 'rows'));
    }


    /**
     * Show view for creating new ad
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAdd()
    {
        $title = 'Creating ad';
        $post = new Ad;
        $countries = Country::all();

        return view('ads.edit', compact([ 'title', 'post', 'countries' ]));
    }

    /**
     * Store new ad in database
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postAdd(Request $request)
    {
        $input = array_merge($request->except( ['_token', 'countries', 'app_file', 'icon'] ));
        $ad = Ad::create( $input );

        $ad->countries()->attach($request->get('countries'));
        $country_list = '';
        $countries = Country::whereIn('id', $request->get('countries'))->get();
        foreach($countries as $country) {
            $country_list .= $country->name . ', ';
        }
        $country_list = substr($country_list, 0, -2);
        if( $file = $request->file('icon') ) {
            Ad::saveFile($file, '', $ad->id, false, 'icon');
        }

        if( $file = $request->file('app_file') ) {
            Ad::saveFile($file, $request->input('apk_name'), $ad->id, false, 'file');
            \Flash::success('<div style="font-size: 12pt">Ad with app <span class="label label-info">' . $ad->name . '</span> has been added successfully for countries: ' . $country_list . '</div>');
        } else {
            \Flash::success('<div style="font-size: 12pt">Ad with slug <span class="label label-info">' . $ad->slug . '</span> has been added successfully for countries: ' . $country_list . '</div>');
        }
        $ad = Ad::findOrFail($ad->id);
        $this->dispatch(new SendPushNotifications($ad));

        return redirect('/ads');
    }

    /**
     * Show ad for edit
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($id)
    {
        $title = "Editing ad";
        $post = Ad::findOrFail($id);
        $countries = Country::all();

        return view('ads.edit', compact(['title', 'post', 'countries']));
    }

    /**
     * Update ad
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEdit(Request $request, $id)
    {
        $ad = Ad::findOrFail($id);
        $ad->update( $request->except('countries', 'app_file', 'slug', 'icon') );

        $ad->countries()->sync($request->get('countries'));
        if( $file = $request->file('icon') ) {
            Ad::saveFile($file, '', $ad->id, true, 'icon');
        }
        $slug = $ad->slug;
        if( $request->input('type') == 'app') {
            if( $file = $request->file('app_file') ) {
                Ad::saveFile( $file, $request->input('apk_name'), $ad->id, true, 'file' );
            } elseif( str_replace('.apk', '', $slug) != $request->input('apk_name') ) {
                $dir = 'uploads/';
                \File::move( $dir . $ad->slug, $dir . $request->input('apk_name') . '.apk' );
                $ad->update( [ 'slug' => $dir . $request->input('apk_name') . '.apk' ] );
            }
            \Flash::success('<div style="font-size: 12pt">Ad with app <span class="label label-info">' . $ad->name . '</span> has successfully updated</div>');
        } else {
            $ad->update(['slug' => $request->input('slug')]);
            \Flash::success('<div style="font-size: 12pt">Ad <span class="label label-info">' . $ad->slug . '</span> has successfully updated</div>');
        }

        return redirect()->back();
    }

    /**
     * Delete ad
     *
     * @param $id
     */
    public function deleteEdit($id)
    {
        Ad::destroy($id);
    }
}
