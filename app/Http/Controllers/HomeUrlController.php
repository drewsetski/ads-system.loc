<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\HomeUrl;
use App\Models\Settings;
use Illuminate\Http\Request;

use App\Http\Requests;

class HomeUrlController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show view for ads
     */
    public function getIndex()
    {
        $title = "Home urls";
        $settings = Settings::first();
        $url_count = HomeUrl::count();

        return view('homeurls/homeurls', compact(['title', 'url_count', 'settings']));
    }

    /**
     * Get all ads
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGet(Request $request)
    {
        $offset = $request->get('offset');
        $limit = $request->get('limit');
        $search = $request->get('search');
        if($search) {
            $rows = HomeUrl::whereHas('countries', function ($query) use ($search) {
                $query->where('name', 'like', '%' . $search . '%')
                    ->orWhere('code', 'like', '%' . $search . '%');
            })
                ->orWhere('homeurls.slug', 'like', '%' . $search . '%')
                ->with(array('countries' =>function($query){
                    $query->select('name');
                }))
                ->orderBy('created_at','desc')
                ->skip($offset)
                ->take($limit)
                ->get();
            $total = count($rows);
        } else {
            $rows = HomeUrl::with(array('countries' =>function($query){
                $query->select('name');
            }))
                ->orderBy('created_at','desc')
                ->skip($offset)
                ->take($limit)
                ->get();
            $total = HomeUrl::all()->count();
        }

        return response()->json(compact('total', 'rows'));
    }

    /**
     * Show view for creating new home url
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAdd()
    {
        $title = 'Creating home url';
        $post = new HomeUrl;
        $countries = Country::all();
        $selected_countries = \DB::table('homeurl_country')->lists('country_id');

        return view('homeurls.edit', compact([ 'title', 'post', 'countries', 'selected_countries' ]));
    }

    /**
     * Store new home url in database
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postAdd(Request $request)
    {
        $input = array_merge($request->except( ['_token', 'countries'] ));
        $homeUrl = HomeUrl::create( $input );

        $homeUrl->countries()->attach($request->get('countries'));
        $country_list = '';
        $countries = Country::whereIn('id', $request->get('countries'))->get();
        foreach($countries as $country) {
            $country_list .= $country->name . ', ';
        }
        $country_list = substr($country_list, 0, -2);

        \Flash::success('<div style="font-size: 12pt">Home url with slug <span class="label label-info">' . $homeUrl->slug . '</span> has been added successfully for countries: ' . $country_list . '</div>');

        return redirect('/homeurls');
    }

    /**
     * Show homeurl for edit
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($id)
    {
        $title = "Editing ad";
        $post = HomeUrl::findOrFail($id);
        $countries = Country::all();
        $selected_countries = \DB::table('homeurl_country')->lists('country_id');

        return view('homeurls.edit', compact(['title', 'post', 'countries', 'selected_countries']));
    }

    /**
     * Update ad
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEdit(Request $request, $id)
    {
        $homeUrl = HomeUrl::findOrFail($id);
        $homeUrl->update( $request->except('countries') );

        $homeUrl->countries()->sync($request->get('countries'));

        \Flash::success('<div style="font-size: 12pt">Home url <span class="label label-info">' . $homeUrl->slug . '</span> has successfully updated</div>');

        return redirect()->back();
    }

    /**
     * Delete ad
     *
     * @param $id
     */
    public function deleteEdit($id)
    {
        HomeUrl::destroy($id);
    }
}
