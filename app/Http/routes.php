<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::get('/', 'HomeController@index');
    Route::get('/getStats', 'HomeController@getStats');

    Route::controllers([
        'ads'                =>      'AdsController',
        'homeurls'           =>      'HomeUrlController',
        'overlays'           =>      'OverlayController',
    ]);

    Route::get('settings', 'SettingsController@index');
    Route::put('settings', 'SettingsController@update');

    /**
     * ALERTS
     */
    Route::get('alerts', 'AlertsController@index');
    Route::get('alerts/all', 'AlertsController@alerts');
    Route::get('alerts/edit/{alert}', 'AlertsController@edit');
    Route::get('alerts/add', 'AlertsController@create');
    Route::post('alerts/add', 'AlertsController@store');
    Route::post('alerts/edit/{alert}', 'AlertsController@update');
    Route::delete('alerts/edit/{alert}', 'AlertsController@delete');

    /**
     * REDIRECTS
     */
    Route::get('redirects', 'RedirectsController@index');
    Route::get('redirects/all', 'RedirectsController@redirects');
    Route::get('redirects/add', 'RedirectsController@create');
    Route::get('redirects/edit/{redirect}', 'RedirectsController@edit');
    Route::post('redirects/add', 'RedirectsController@store');
    Route::post('redirects/edit/{redirect}', 'RedirectsController@update');
    Route::delete('redirects/edit/{redirect}', 'RedirectsController@delete');

    /**
     * API
     */
    Route::group(['prefix' => 'api/v1'], function()
    {
        Route::post('device', 'api\MainController@device');
        Route::get('homeurl', 'api\MainController@homeurl');
        Route::get('alerts', 'api\MainController@alerts');
        Route::get('overlay', 'api\MainController@overlay');
        Route::get('redirects', 'api\MainController@redirects');
        Route::get('all', 'api\MainController@getAll');
    });
});
