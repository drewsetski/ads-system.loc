<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\Device;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use PushNotification;

class SendPushNotifications extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $ad;
    /**
     * Create a new job instance.
     * @param $ad
     */
    public function __construct($ad)
    {
        $this->ad = $ad;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $ad = $this->ad;
        $deviceArray = [];
        $appName = "appNameAndroid";
        $slug = $ad->type == 'link' ? $ad->slug : $ad->app_slug;
        if($ad->type == 'link') {
            $message = PushNotification::Message($ad->type,array(
                'sound' => 'default',
                'custom' => array(
                    'type' => $ad->type,
                    'slug' => $slug,
                    'icon' => $ad->icon_image,
                    'name' => $ad->name,
                    'text' => $ad->text,
                ),
            ));
        } else {
            $message = PushNotification::Message($ad->type,array(
                'sound' => 'default',
                'custom' => array(
                    'type' => $ad->type,
                    'slug' => $slug,
                    'icon' => $ad->icon_image,
                    'name' => $ad->name,
                    'text' => $ad->text,
                    'apk_name' => $ad->apk_name,
                ),
            ));
        }

        $countries = $ad->countries()->lists('code')->toArray();

        $devices = Device::whereNotNull('device_token')->whereDeviceType('android')->whereIn('country_code', $countries)->get();

        foreach($devices as $user){
            array_push($deviceArray, PushNotification::Device($user->device_token));
        }

        $devices = PushNotification::DeviceCollection($deviceArray);

        $response = PushNotification::app($appName)
            ->to($devices)
            ->send($message);
    }
}
