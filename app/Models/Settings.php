<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $table = 'settings';

    protected $fillable = ['reopening_time', 'redirects_periodicity', 'landings_periodicity'];

    public $timestamps = false;
}
