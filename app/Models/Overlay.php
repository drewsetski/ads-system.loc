<?php

namespace App\Models;

use File;
use Illuminate\Database\Eloquent\Model;

class Overlay extends Model
{
    protected $table = 'overlays';

    protected $fillable = ['slug', 'banner', 'text', 'periodicity'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['banner_image'];

    /**
     * Overlay belongs to many countries
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function countries()
    {
        return $this->belongsToMany('App\Models\Country', 'overlay_country', 'overlay_id');
    }

    public function getBannerImageAttribute()
    {
        return url('/uploads/' . $this->attributes['banner'] );
    }

    /**
     * Upload apk file
     *
     * @param $file
     * @param integer $id
     * @param bool $update
     */
    public static function saveFile($file, $id, $update = false)
    {
        $overlay = self::find( $id );
        $dir = 'uploads/';
        $name = 'banner';
        if($update) {
            $old_filename = $overlay->banner;
            File::delete( $dir . $old_filename );
        }
        $filename  = $name . '_' . time() . '.' . $file->getClientOriginalExtension();

        $file->move($dir, $filename);

        self::find( $id )->update(['banner' => $filename]);
    }

    /**
     * Destroy ad
     *
     * @param array|int $id
     * @return int|void
     */
    public static function destroy($id)
    {
        $overlay = self::find( $id );
        $dir =  'uploads/';
        File::delete( $dir . $overlay->banner );

        parent::destroy( $id );
    }
}
