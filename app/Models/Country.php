<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';

    /**
     * Country has many ads
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ads()
    {
        return $this->belongsToMany('App\Models\Ad', 'ad_country');
    }

    /**
     * Country has many alerts
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function alerts()
    {
        return $this->belongsToMany('App\Models\Alert', 'alert_country');
    }

    /**
     * Country has ove home url
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function homeurl()
    {
        return $this->belongsToMany('App\Models\HomeUrl', 'homeurl_country', 'country_id');
    }

    /**
     * Country has one overlay
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function overlay()
    {
        return $this->belongsToMany('App\Models\Overlay', 'overlay_country', 'country_id');
    }

    /**
     * Country can have many redirects
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function redirects()
    {
        return $this->hasMany(Redirect::class);
    }
}
