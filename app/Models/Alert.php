<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{
    protected $table = 'alerts';

    protected $fillable = ['text', 'periodicity', 'slug', 'button_text'];

    /**
     * Alert belongs to many countries
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function countries()
    {
        return $this->belongsToMany('App\Models\Country', 'alert_country');
    }


}
