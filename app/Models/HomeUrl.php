<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomeUrl extends Model
{
    protected $table = 'homeurls';

    protected $fillable = ['slug'];

    /**
     * Homeurl belongs to many countries
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function countries()
    {
        return $this->belongsToMany('App\Models\Country', 'homeurl_country', 'homeurl_id');
    }
}
