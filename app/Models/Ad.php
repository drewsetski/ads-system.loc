<?php

namespace App\Models;

use File;
use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    protected $table = 'ads';

    protected $fillable = ['slug', 'name', 'type', 'icon', 'text', 'apk_name'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['app_slug', 'icon_image'];

    /**
     * Ad belongs to many countries
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function countries()
    {
        return $this->belongsToMany('App\Models\Country', 'ad_country');
    }

    public function getAppSlugAttribute()
    {
        if($this->attributes['type'] == 'app')
            return url('/uploads/' . $this->attributes['slug'] );
    }
    public function getIconImageAttribute()
    {
        return url('/uploads/' . $this->attributes['icon'] );
    }

    /**
     * Upload apk file
     *
     * @param $file
     * @param $apk_name
     * @param integer $id
     * @param bool $update
     * @param string $type
     */
    public static function saveFile($file, $apk_name, $id, $update = false, $type)
    {
        $ad = self::find( $id );
        $dir = 'uploads/';
        $filename = '';
        switch ($type) {
            case 'file':
                $column = 'slug';
                $name = 'app';
                if(strlen($apk_name) > 0) {
                    $filename = $apk_name . '.' . $file->getClientOriginalExtension();
                } else {
                    $filename = $name . '_' . time() . '.' . $file->getClientOriginalExtension();
                }
                break;
            case 'icon':
                $column = 'icon';
                $filename = 'icon' . '_' . time() . '.' . $file->getClientOriginalExtension();
                break;
        }
        if($update) {
            switch ($type) {
                case 'file':
                    $old_filename = $ad->slug;
                    break;
                case 'icon':
                    $old_filename = $ad->icon;
                    break;
            }

            File::delete( $dir . $old_filename );
        }

        $file->move($dir, $filename);

        self::find( $id )->update([$column => $filename]);
    }

    /**
     * Destroy ad
     *
     * @param array|int $id
     * @return int|void
     */
    public static function destroy($id)
    {
        $ad = self::find( $id );
        $dir =  'uploads/';
        if($ad->type == 'app') {
            $old_filename = $ad->slug;
            File::delete( $dir . $old_filename );
        }
        File::delete( $dir . $ad->icon );

        parent::destroy( $id );
    }

}
