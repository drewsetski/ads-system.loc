<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $table = 'devices';

    protected $fillable = ['device_type', 'device_token', 'country_code', 'updated_at'];


    /**
     * Get installed apps
     *
     * @param $query
     * @return mixed
     */
    public function scopeInstalled($query)
    {
        $now = Carbon::now();
        $dayBefore = $now->subDay();
        return $query->where('updated_at', '>=', $dayBefore);
    }

    /**
     * Get deleted apps
     *
     * @param $query
     * @return mixed
     */
    public function scopeRemoved($query)
    {
        $now = Carbon::now();
        $dayBefore = $now->subDay();
        return $query->where('updated_at', '<', $dayBefore);
    }
}
