@extends('layout')

@section('content')
    <main>
        <div class="page-header">
            <h1>{!! $title !!}</h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                    {!! Form::model($settings, ['method' => 'PUT', 'class' => 'form-horizontal', 'role' => 'form']) !!}
                        <div class="form-group">
                            <!--- Reopening Time Field --->
                            {!! Form::label('reopening_time', 'Reopening Time:', ['class' => 'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::input('number', 'reopening_time', null, [
                                    'class' => 'form-control',
                                    'required',
                                    'min' => 0,
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'auto bottom',
                                    'title' => 'Time in seconds, after which app will reopen',
                                ]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <!--- Reopening Time Field --->
                            {!! Form::label('redirects_periodicity', 'Periodicity for redirects:', ['class' => 'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::input('number', 'redirects_periodicity', null, [
                                    'class' => 'form-control',
                                    'required',
                                    'min' => 0,
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'auto bottom',
                                    'title' => 'Time in seconds, after which redirect fires',
                                ]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <!--- Reopening Time Field --->
                            {!! Form::label('landings_periodicity', 'Periodicity for landings:', ['class' => 'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {!! Form::input('number', 'landings_periodicity', null, [
                                    'class' => 'form-control',
                                    'required',
                                    'min' => 0,
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'auto bottom',
                                    'title' => 'Time in seconds, after which landing will open',
                                ]) !!}
                            </div>
                        </div>
                        <button type="submit" id="submit" class="btn btn-success btn-flat"><i class="fa fa-floppy-o fa-2x"></i></button>
                    {!! Form::close() !!}
                </div>
            </div>

        </div> <!-- end of content -->
    </main> <!-- end of main -->
@endsection

