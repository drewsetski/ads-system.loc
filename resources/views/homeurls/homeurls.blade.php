@extends('layout')

@section('content')
    <main>
        <a href="{!! url('/homeurls/add') !!}" class="btn btn-brand btn-flat"><i class="fa fa-plus fa-2x"></i></a>
        <div class="page-header">
            <h1>{!! $title !!}</h1>
        </div>
        <div class="content">
            <div class="form-group">
                <!--- Reopening Time Field --->
                {!! Form::label('landings_periodicity', 'Periodicity for landings:', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-2">
                    {!! Form::input('number', 'landings_periodicity', $settings->landings_periodicity, [
                        'class' => 'form-control',
                        'required',
                        'min' => 0,
                        'data-toggle' => 'tooltip',
                        'data-placement' => 'auto bottom',
                        'title' => 'Time in seconds, after which redirect fires',
                    ]) !!}
                </div>
                <div class="col-sm-2"><button class="btn btn-success" id="btn-update-settings">Update</button></div>
            </div>
            <div class="clearfix mb25"></div>
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                    @if($url_count > 0)
                        <div class="row table-responsive" style="width: auto;">
                            <table id="apps-table"
                                   data-route="/homeurls"
                                   data-url="/homeurls/get"
                                   data-toggle="table"
                                   data-page-size="10"
                                   data-search="true"
                                   data-side-pagination="server"
                                   data-pagination="true"
                                   data-query-params="queryParams">
                                <thead>
                                <tr>
                                    <th data-field="slug">Link</th>
                                    <th data-field="countries" data-formatter="countries">Countries</th>
                                    <th data-field="id" data-formatter="actions" data-width="100px">Actions</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    @else
                        <div class="alert empty warning">
                            There are no home urls
                        </div>
                    @endif
                </div>
            </div>

        </div> <!-- end of content -->
    </main> <!-- end of main -->
@endsection

