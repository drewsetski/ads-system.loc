@extends('layout')

@section('content')
    <main>
        <div class="page-header">
            <h1 class="inline"><a href="{{ url('homeurls') }}" class="back"><i class="fa fa-chevron-circle-left"></i></a>{!! $title !!}</h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                    {!! Form::open(array( 'class' => 'form-horizontal', 'role' => 'form') ) !!}
                        <div class="form-group">
                            {!! Form::label("Countries", '', array('class'=>'col-sm-2 control-label') ) !!}
                            <div class="col-sm-10">
                                <select multiple="multiple" id="countries" name="countries[]" class="chosen-select form-control" data-placeholder="Select countries where you want to show your ad">
                                    @foreach( $countries as $country )
                                        <option value='{{ $country->id }}' @if( in_array( $country->id, $post->countries()->lists('id')->toArray() ) ) selected @endif  @if( in_array( $country->id, $selected_countries ) && !in_array($country->id, $post->countries()->lists('id')->toArray()) ) disabled @endif>{{ $country->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label("slug", 'Slug', array('class'=>'col-sm-2 control-label') ) !!}
                            <div class="col-sm-10">
                                {!! Form::url('slug',  $post->slug, array('class'=>'form-control') ) !!}
                            </div>
                        </div>
                        <button type="submit" id="submit" class="btn btn-success btn-flat"><i class="fa fa-floppy-o fa-2x"></i></button>
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </main>
@endsection
@section('scripts')

@endsection