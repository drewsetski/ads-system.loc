@extends('layout')

@section('content')
    <main>
        <div class="page-header">
            <h1>{!! $title !!}</h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                    <div class="row">
                        <div class='col-md-2'>
                            <div class="form-group">
                                <label>Date</label>
                                <div class='input-group date' id='date'>
                                    <input type='text' class="form-control" name="date" value="{!! $date !!}"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="legend">
                            <ul class="pie-legend">
                                <li>
                                    <span style="background-color:#4caf50"></span>Installed - <i id="installed"></i>
                                </li>
                                <li>
                                    <span style="background-color:#F7464A"></span>Removed - <i id="removed"></i>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-10">
                        <canvas id="statsChart" width="400" height="400"></canvas>
                    </div>
                </div>
            </div>

        </div> <!-- end of content -->
    </main> <!-- end of main -->
@endsection

@section('styles')
    <link rel="stylesheet" href="/css/bootstrap-datetimepicker.min.css">
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
    <script src="/js/moment.min.js"></script>
    <script src="/js/bootstrap-datetimepicker.min.js"></script>
    <script>
        window.chartOptions = {
            segmentShowStroke : true,
            segmentStrokeColor : "#fff",
            segmentStrokeWidth : 1,
            animationSteps : 50,
            animationEasing : "easeOutBounce",
            animateRotate : true,
            animateScale : false,
        };
        var chartUpdate = function(installed, removed) {
            // Replace the chart canvas element
            $('#statsChart').replaceWith('<canvas id="statsChart" width="400" height="400"></canvas>');

            // Draw the chart
            var ctx = $("#statsChart").get(0).getContext("2d");
            var pieChart = new Chart(ctx).Pie([
                        {
                            value: installed,
                            color:"#4caf50",
                            highlight: "#4fbc52",
                            label: "Installed"
                        },
                        {
                            value: removed,
                            color: "#F7464A",
                            highlight: "#FF5A5E",
                            label: "Deleted"
                        },],
                    window.chartOptions);
            $('#installed').html(installed);
            $('#removed').html(removed);
        }
        var getStats = function() {
            $.get('/getStats',
                    {
                        date: $("#date input").val(),
                    })
                    .done(function(data) {
                        chartUpdate(data['installed'], data['removed']);
                    });
        }
        $(function () {
            getStats();
            $('#date').datetimepicker({
                format: 'YYYY-MM-DD',
                maxDate: moment().format('YYYY-MM-DD')
            });
            $("#date").on("dp.change", function (e) {
                getStats();
            });
        });

    </script>
@endsection