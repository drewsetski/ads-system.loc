<html>
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{!! csrf_token() !!}" />
    @if ( isset( $title ) ) <title>{!! $title !!}</title>@endif
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/jquery.navobile.css" rel="stylesheet">
    <link href="/css/animate.min.css" rel="stylesheet">
    <link href="/css/check.css" rel="stylesheet">
    <link href="/css/switchery.min.css" rel="stylesheet"/>
    <link href="/css/chosen.css" rel="stylesheet"/>
    <link href="/css/jquery.jgrowl.min.css" rel="stylesheet"/>
    <link href="/css/bootstrap-table.min.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/other.css" rel="stylesheet">

    @yield('styles')

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="wrapper" id="content">
    <header class="header">
        <nav class="header-nav">
            <ul class="f-left">
                <li class="f-left">
                    <a href="#" id="show-sidebar" class="animated slideInLeftSmall"><i class="fa fa-bars fa-2x"></i></a>
                </li>
            </ul>
            <ul class="mainnav f-right">
                <li><a href="{{ url('/logout') }}" data-toggle="tooltip" data-placement="bottom" title="Log out"><i class="fa fa-sign-out fa-2x"></i></a></li>
            </ul>
        </nav>
    </header><!-- .header-->

    <nav class="left-sidebar">
        <div class="sidebar-container">
            <div class="logo"><a href="/">{!! env('PROJECT_NAME') !!}</a></div>
            <ul>
                <li {{ Request::is('/') ? ' class=active' : null }}><a href="/"><i class="fa fa-pie-chart fa-2x"></i><span>Stats</span></a></li>
                <li {{ Request::segment(1) == 'settings' ? ' class=active' : null }}><a href="{{ url('/settings') }}"><i class="fa fa-cogs fa-2x"></i><span>Settings</span></a></li>
                <li {{ Request::segment(1) == 'homeurls' ? ' class=active' : null }}><a href="{{ url('/homeurls') }}"><i class="fa fa-home fa-2x"></i><span>Home urls</span></a></li>
                <li {{ Request::segment(1) == 'overlays' ? ' class=active' : null }}><a href="{{ url('/overlays') }}"><i class="fa fa-picture-o fa-2x"></i><span>Overlays</span></a></li>
                <li {{ Request::segment(1) == 'ads' ? ' class=active' : null }}><a href="{{ url('/ads') }}"><i class="fa fa-link fa-2x"></i><span>Shortcuts & Notification APKs</span></a></li>
                <li {{ Request::segment(1) == 'alerts' ? ' class=active' : null }}><a href="{{ url('/alerts') }}"><i class="fa fa-exclamation-triangle fa-2x"></i><span>Alerts</span></a></li>
                <li {{ Request::segment(1) == 'redirects' ? ' class=active' : null }}><a href="{{ url('/redirects') }}"><i class="fa fa-external-link-square fa-2x"></i><span>Redirects</span></a></li>
            </ul>
        </div>

        @include('partials._footer')
    </nav>

    @yield('content')

</div><!-- end of wrapper -->

    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/bootstrap-filestyle.min.js"></script>
    <script src="/js/bootbox.js"></script>
    <script src="/js/jquery.navobile.min.js"></script>
    <script src="/js/switchery.min.js"></script>
    <script src="/js/chosen.jquery.min.js"></script>
    <script src="/js/jquery.jgrowl.min.js"></script>
    <script src="/js/bootstrap-table.min.js"></script>
    <script src="/js/app.js"></script>
    <script src="/js/main.js"></script>
    @yield('scripts')
</body>
</html>