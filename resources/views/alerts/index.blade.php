@extends('layout')

@section('content')
    <main>
        <a href="{!! url('/alerts/add') !!}" class="btn btn-brand btn-flat"><i class="fa fa-plus fa-2x"></i></a>
        <div class="page-header">
            <h1>{!! $title !!}</h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                    @if($alerts_count > 0)
                        <div class="row table-responsive" style="width: auto;">
                            <table id="apps-table"
                                   data-route="/alerts"
                                   data-url="/alerts/all"
                                   data-toggle="table"
                                   data-page-size="10"
                                   data-search="true"
                                   data-side-pagination="server"
                                   data-pagination="true"
                                   data-query-params="queryParams">
                                <thead>
                                <tr>
                                    <th data-field="text">Text</th>
                                    <th data-field="slug">Slug</th>
                                    <th data-field="periodicity">Periodicity</th>
                                    <th data-field="countries" data-formatter="countries">Countries</th>
                                    <th data-field="id" data-formatter="actions" data-width="100px">Actions</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    @else
                        <div class="alert empty warning">
                            There are no alerts
                        </div>
                    @endif
                </div>
            </div>

        </div> <!-- end of content -->
    </main> <!-- end of main -->
@endsection

