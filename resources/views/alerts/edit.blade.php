@extends('layout')

@section('content')
    <main>
        <div class="page-header">
            <h1>{!! $title !!}</h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                    {!! Form::model($alert, ['url' => $url, 'method' => 'POST', 'class' => 'form-horizontal', 'role' => 'form']) !!}
                    <div class="form-group">
                        <!-- Text on alert -->
                        {!! Form::label('text', 'Alert text:', ['class' => 'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::text('text', null, [ 'class' => 'form-control', 'required',]) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <!-- Text on button of alert -->
                        {!! Form::label('button_text', 'Button text:', ['class' => 'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::text('button_text', null, [ 'class' => 'form-control', 'required',]) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label("slug", 'Slug', array('class'=>'col-sm-2 control-label') ) !!}
                        <div class="col-sm-10">
                            {!! Form::url('slug',  null, array('class'=>'form-control', 'required') ) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <!-- Reopening Time Field -->
                        {!! Form::label('periodicity', 'Periodicity:', ['class' => 'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::input('number', 'periodicity', null, [
                                'class' => 'form-control',
                                'min' => 0,
                                'required',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'auto bottom',
                                'title' => 'Time in seconds, after which alert will appear',
                            ]) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label("Countries", '', array('class'=>'col-sm-2 control-label') ) !!}
                        <div class="col-sm-10">
                            <select multiple="multiple" id="countries" name="countries[]" class="chosen-select form-control" data-placeholder="Select countries where you want to show your ad">
                                @foreach( $countries as $country )
                                    <option value='{{ $country->id }}' @if( in_array( $country->id, $alert->countries()->lists('id')->toArray() ) ) selected @endif>{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <button type="submit" id="submit" class="btn btn-success btn-flat"><i class="fa fa-floppy-o fa-2x"></i></button>
                    {!! Form::close() !!}
                </div>
            </div>

        </div> <!-- end of content -->
    </main> <!-- end of main -->
@endsection

