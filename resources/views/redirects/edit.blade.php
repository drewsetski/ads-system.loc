@extends('layout')

@section('content')
    <main>
        <div class="page-header">
            <h1>{!! $title !!}</h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                    {!! Form::model($redirect, ['url' => $url, 'method' => 'POST', 'class' => 'form-horizontal', 'role' => 'form']) !!}

                    <div class="form-group">
                        {!! Form::label("country", 'Country: ', ['class'=>'col-sm-2 control-label'] ) !!}
                        <div class="col-sm-10">
                            {!! Form::select('country', $countries, $redirect != null ? $redirect->country_id : null, [
                                'id' => 'countries',
                                'class' => 'chosen-select form-control',
                                'data-placeholder' => 'Select countries where you want to show your ad'
                            ]) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <!--- Link Field --->
                        {!! Form::label('link', 'Link:', ['class' => 'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::input('url', 'link', null, [ 'class' => 'form-control', 'required',]) !!}
                        </div>
                    </div>
                    <button type="submit" id="submit" class="btn btn-success btn-flat"><i class="fa fa-floppy-o fa-2x"></i></button>
                    {!! Form::close() !!}
                </div>
            </div>

        </div> <!-- end of content -->
    </main> <!-- end of main -->
@endsection

