@extends('layout')

@section('content')
    <main>
        <div class="page-header">
            <h1 class="inline"><a href="{{ url('ads') }}" class="back"><i class="fa fa-chevron-circle-left"></i></a>{!! $title !!}</h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                    {!! Form::open(array( 'class' => 'form-horizontal', 'role' => 'form', 'files' => true) ) !!}
                        <div class="form-group">
                            {!! Form::label('type', 'Type of ad', array('class'=>'col-sm-2 control-label') ) !!}
                            <div class="col-sm-10">
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-success {!! $post->type == 'link' ? "active" : "" !!}">
                                        <input name="type" value="link" type="radio" {!! $post->type == 'link' ? "checked" : "" !!}>
                                        <i class="fa fa-link fa-3x"></i>
                                    </label>
                                    <label class="btn btn-success {!! $post->type == 'app' ? "active" : "" !!}">
                                        <input name="type" value="app" type="radio" {!! $post->type == 'app' ? "checked" : "" !!}>
                                        <i class="fa fa-android fa-3x"></i>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('icon', 'Icon', array('class'=>'col-sm-2 control-label') ) !!}
                            @if($post->icon)
                                <div class="col-sm-1">
                                    <img style="max-width:100%" src="/uploads/{!! $post->icon !!}" alt="Icon image" id="icon-image">
                                </div>
                            @endif
                            <div class="col-sm-9" data-toggle="tooltip" data-placement="top">
                                {!! Form::file('icon', array('class' => 'filestyle', 'data-value' => $post->icon, 'data-buttonName' => 'btn-brand', 'data-icon' => 'true') ) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label("Countries", '', array('class'=>'col-sm-2 control-label') ) !!}
                            <div class="col-sm-10">
                                <select multiple="multiple" id="countries" name="countries[]" class="chosen-select form-control" data-placeholder="Select countries where you want to show your ad">
                                    @foreach( $countries as $country )
                                        <option value='{{ $country->id }}' @if( in_array( $country->id, $post->countries()->lists('id')->toArray() ) ) selected @endif>{{ $country->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('name', 'Name', array('class'=>'col-sm-2 control-label') ) !!}
                            <div class="col-sm-10">
                                {!! Form::text('name',  $post->name, array('class'=>'form-control', 'required') ) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('text', 'Text', array('class'=>'col-sm-2 control-label') ) !!}
                            <div class="col-sm-10">
                                {!! Form::text('text',  $post->text, array('class'=>'form-control') ) !!}
                            </div>
                        </div>
                        <div id="app" class="{!! $post->type == 'app' ? "" : "none" !!}">
                            <div class="form-group">
                                {!! Form::label('app_file', 'App\'s file *.apk', array('class'=>'col-sm-2 control-label') ) !!}
                                <div class="col-sm-10" data-toggle="tooltip" data-placement="top">
                                    {!! Form::file('app_file', array('class' => 'filestyle', 'data-value' => $post->slug, 'data-buttonName' => 'btn-brand', 'data-icon' => 'true') ) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('apk_name', 'Apk filename', array('class'=>'col-sm-2 control-label') ) !!}
                                <div class="col-sm-10">
                                    {!! Form::text('apk_name',  $post->apk_name, array('class'=>'form-control') ) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group slug {!! $post->type == 'link' ? "" : "none" !!}">
                            {!! Form::label("Slug", '', array('class'=>'col-sm-2 control-label') ) !!}
                            <div class="col-sm-10">
                                {!! Form::url('slug',  $post->slug, array('class'=>'form-control') ) !!}
                            </div>
                        </div>
                        <button type="submit" id="submit" class="btn btn-success btn-flat none"><i class="fa fa-floppy-o fa-2x"></i></button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </main>
@endsection

@section('scripts')
    <script>
        $(function(){
            switch ("{!! $post->type !!}") {
                case 'link':
                    $('.slug').removeClass('none');
                    $('#app').addClass('none');
                    $('input[name="slug"]').prop('required', true);
                    $('input[name="apk_name"]').val('').prop('required', false);
                    $(':file[name="app_file"]').filestyle('clear');
                    $('#submit').show();
                    break;
                case 'app':
                    $('.slug').addClass('none');
                    $('#app').removeClass('none');
                    $('input[name="apk_name"]').prop('required', true);
                    $('input[name="slug"]').val('').prop('required', false);
                    $('#submit').show();
                    break;
            }
            $('input[name="type"]').change(function(){
                switch ($(this).val()) {
                    case 'link':
                        $('.slug').removeClass('none');
                        $('#app').addClass('none');
                        $('input[name="slug"]').prop('required', true);
                        $('input[name="apk_name"]').val('').prop('required', false);
                        $(':file[name="app_file"]').filestyle('clear');
                        $('#submit').show();
                        break;
                    case 'app':
                        $('.slug').addClass('none');
                        $('#app').removeClass('none');
                        $('input[name="apk_name"]').prop('required', true);
                        $('input[name="slug"]').val('').prop('required', false);
                        $('#submit').show();
                        break;
                }
                $('.package-group').removeClass('none');
            });
        });
    </script>
@endsection