@extends('layout')

@section('content')
    <main>
        <a href="{!! url('/ads/add') !!}" class="btn btn-brand btn-flat"><i class="fa fa-plus fa-2x"></i></a>
        <div class="page-header">
            <h1>{!! $title !!}</h1>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('flash::message')
                    @if($ads_count > 0)
                        <div class="row table-responsive" style="width: auto;">
                            <table id="apps-table"
                                   data-route="/ads"
                                   data-url="/ads/get"
                                   data-toggle="table"
                                   data-page-size="10"
                                   data-search="true"
                                   data-side-pagination="server"
                                   data-pagination="true"
                                   data-query-params="queryParams">
                                <thead>
                                <tr>
                                    <th data-field="icon_image" data-formatter="image" data-width="20%" data-align="center">Icon</th>
                                    <th data-field="name">Name</th>
                                    <th data-field="slug">Link</th>
                                    <th data-field="countries" data-formatter="countries">Countries</th>
                                    <th data-field="id" data-formatter="actions" data-width="100px">Actions</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    @else
                        <div class="alert empty warning">
                            There are no ads
                        </div>
                    @endif
                </div>
            </div>

        </div> <!-- end of content -->
    </main> <!-- end of main -->
@endsection

