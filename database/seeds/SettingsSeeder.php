<?php

use App\Models\Settings;
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Settings::create([
            'reopening_time' => 600,
            'redirects_periodicity' => 100,
            'landings_periodicity' => 100,
        ]);
    }
}
