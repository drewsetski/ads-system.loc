<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // creating admin user
        User::create([
            'name' => 'Admin',
            'email' => "admin@ads.com",
            'password' => bcrypt("123123"),
        ]);
    }
}
