<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            // time in seconds, after which app will reopen
            $table->integer('reopening_time', false, true);
            // periodicity for redirects
            $table->integer('redirects_periodicity', false, true);
            // periodicity for landings
            $table->integer('landings_periodicity', false, true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('settings');
    }
}
