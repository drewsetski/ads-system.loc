<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeurlCountryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homeurl_country', function (Blueprint $table) {
            $table->integer('homeurl_id')->unsigned()->index();
            $table->integer('country_id')->unique()->unsigned()->index();

            $table->foreign('homeurl_id')
                ->references('id')
                ->on('homeurls')
                ->onDelete('cascade');

            $table->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('homeurl_country');
    }
}
