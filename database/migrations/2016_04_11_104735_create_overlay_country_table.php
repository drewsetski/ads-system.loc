<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOverlayCountryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('overlay_country', function (Blueprint $table) {
            $table->integer('overlay_id')->unsigned()->index();
            $table->integer('country_id')->unique()->unsigned()->index();

            $table->foreign('overlay_id')
                ->references('id')
                ->on('overlays')
                ->onDelete('cascade');

            $table->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('overlay_country');
    }
}
