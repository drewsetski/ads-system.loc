/**
 * Setting csrf token header for ajax requests
 */
$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });

/**
 * Query params function for server side pagination
 *
 * @param params
 * @returns {{limit: *, offset: *, search: *}}
 */
function queryParams(params) {
    return {
        limit: params.limit,
        offset: params.offset,
        search: params.search
    };
}

$('#countries').chosen();
var table, route;

$(document).ready(function () {
    table = $('.table-responsive').find('table.table.table-hover');
    route = table.attr('data-route');
});

/**
 * Formatter for tale actions button
 * edit/delete
 *
 * @param id
 */
function actions(id) {
    var edit = $('<a title="Edit" href="" class="btn btn-info"><span class="glyphicon glyphicon-pencil"></span></a>');
    edit.attr('href', route + '/edit/' + id );
    var del = $(
        '<button title="Delete" type="button" class="delete btn btn-danger" data-id="' + id + '">' +
        '<span class="glyphicon glyphicon-remove"></span>' +
        '</button>'
    );
    return '<div class="btn-group">' + edit[0].outerHTML + del[0].outerHTML + '</div>';
}
/**
 * Formatter for ad's countries
 *
 * @param countries
 */
function countries(value) {
    var countries = "";
    $.each(value, function(index, val){
        countries += val.name + ", ";
    });
    countries = countries.slice(0,-2);
    return countries;
}
/**
 * Image
 *
 * @param value
 * @returns {string}
 */
function image(value) {
    return '<img src="'+ value +'" class="icon-img"/>'
}

/**
 * Deleting record from table
 */
$('body').on('click', '.delete', function(){
    var id = $(this).attr('data-id');
    bootbox.confirm("Are you sure you want to delete record?", function(result) {
        if(result) {
            $.ajax({
                url: route + '/edit/' + id,
                _token: '{{ Session::token() }}',
                type: 'DELETE',
                success: function(){
                    table.bootstrapTable('refresh');
                },
                error: function(error){
                    $.jGrowl( "Cannot delete this record", {
                        sticky:false,
                        theme: "danger",
                        header: "Error"
                    });
                }
            });
        }
    });
});

/**
 * Update settings
 */
$('#btn-update-settings').click(function(){
    var $input =  $(this).parent().prev().children('input');
    var option = $input.attr('id');
    var value = $input.val();
    console.log(option);
    console.log(value);
    $.ajax({
        type: 'PUT',
        url: "/settings",
        data: {
            option: option,
            value: value,
            _token: $('meta[name=csrf-token]').attr("content")
        },
        success: function (data) {
            $.jGrowl( "Settings have successfully updated", {
                sticky:false,
                theme: "success",
                header: "Success"
            });
        },
        error: function (data) {
            $.jGrowl( data, {
                sticky:false,
                theme: "danger",
                header: "Error"
            });
        }
    });

});